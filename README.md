# **Profiling of open chromatin in developing pig (*Sus scrofa*) muscle to identify regulatory regions** #

*Mazdak Salavati1,2§, Shernae A. Woolley1, Yennifer Cortés Araya1, Michelle M. Halstead3, Claire Stenhouse1,4, Martin Johnsson1,5, Cheryl J. Ashworth1, Alan L. Archibald1, Francesc X. Donadeu1, Musa A. Hassan1,2 and Emily L. Clark1,2,§*

- *1 The Roslin Institute and Royal (Dick) School of Veterinary Studies, The University of Edinburgh, Edinburgh, UK*
- *2 Centre for Tropical Livestock Genetics and Health (CTLGH), Roslin Institute, University of Edinburgh, Easter Bush Campus, EH25 9RG, UK*
- *3 Department of Animal Science, University of California, Davis, California, US*
- *4 Department of Animal Science, Texas A&M University, Texas, USA*
- *5 Swedish University of Agricultural Sciences, Uppsala, Sweden*

### §Corresponding authors: emily.clark@roslin.ed.ac.uk ; mazdak.salavati@roslin.ed.ac.uk 

*DOI: https://doi.org/10.1101/2021.01.21.426812*

*Supp: https://doi.org/10.6084/m9.figshare.13562285*

*ENA project repositories:*

- https://www.ebi.ac.uk/ena/browser/view/PRJEB41488
- https://www.ebi.ac.uk/ena/browser/view/PRJEB41485

*SOPs and protocols available via FAANG's data portal*

- https://data.faang.org/api/fire_api/samples/ROSLIN_SOP_ATAC-Seq_DNAIsolationandTagmentation_Cryopreserved_Muscle_Nuclei_Preparations_20200720.pdf
- https://data.faang.org/api/fire_api/analyses/ROSLIN_SOP_ATAC-Seq_analysis_pipeline_20201113.pdf
- https://data.faang.org/api/fire_api/samples/ROSLIN_SOP_Collection_of_tissue_samples_for_ATAC-Seq_and_RNA-Seq_from_large_animals_20200618.pdf
- https://data.faang.org/api/fire_api/samples/ROSLIN_SOP_RNA_IsolationoftotalRNAfromfrozentissuesamples_20200720.pdf
- https://data.faang.org/api/fire_api/samples/ROSLIN_SOP_ATAC-Seq_LibraryPreparationandSizeSelection_20200720.pdf
- https://data.faang.org/api/fire_api/analyses/ROSLIN_SOP_RNA-Seq_analysis_pipeline_20201113.pdf
- https://data.faang.org/api/fire_api/samples/ROSLIN_SOP_Cryopreservation_of_Nuclei_for_ATACSeq_using_GentleMACS_20201119.pdf
- https://data.faang.org/api/fire_api/samples/ROSLIN_SOP_ATAC_Seq_DNAIsolationandTagmentation_Frozen_Muscle_Tissue_20200720.pdf




### Authors ORCIDs:
- MS	https://orcid.org/0000-0002-7349-2451
- SAW	https://orcid.org/0000-0003-2317-2916
- YCA	https://orcid.org/0000-0002-8456-2152
- MMH	https://orcid.org/0000-0003-0168-2704
- CS	https://orcid.org/0000-0001-8302-7900
- MJ	https://orcid.org/0000-0003-1262-4585
- CJA	https://orcid.org/0000-0002-2607-5831 
- AA	https://orcid.org/0000-0001-9213-1830
- FXD	https://orcid.org/0000-0002-0331-2996
- MAH	https://orcid.org/0000-0002-0371-3300
- ELC	https://orcid.org/0000-0002-9550-7407

=====================================================================================================

This repository contains code for analysis of the following datasets:

1. ATAC-Seq mapping and peak calling (bwa-mem >>>> GenRich)
2. RNA-Seq transcript level quantification (kallisto on Sscrofa 11.1.100 Ensembl genome) 
3. Allele specific expression analysis of the same RNA-Seq dataset (HiSAT2 >>> WASP >>> GATK ASEReadCounter >>> GeneiASE) 
4. Collection of custom scripts used in the supplementary files 1 and 2 analysis. 
5. ATAC-Seq differential peak analysis (htseq) and TF footprinting (HINT pipeline)
