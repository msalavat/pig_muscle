#!/bin/bash
#This is the script for the analysis 

#SGE flags
#$ -N PIG0003_AN3_1													##TBC1
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=30G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 1
#$ -V
#$ -t 1-26						##TBC


#Abs paths
REF=/YOURFOLDERSTRUCTURE/workplace_pig
REF2=/YOURFOLDERSTRUCTURE/workplace_pig/ref/Sscrofa11.1.100
SOURCE=/YOURFOLDERSTRUCTURE/bams
mkdir -p /YOURFOLDERSTRUCTURE/$USER/PIG0003						##TBC2
WASP=/home/$USER/tools/WASP
DESTINATION=/YOURFOLDERSTRUCTURE/$USER/PIG0003					##TBC3

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/PIG0003_output								##TBC5
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=/exports/eddie/scratch/$USER/bams							##TBC6
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/PIG0003_output								##TBC7

#Loading modules
module load roslin/fastqc/0.11.7
module load igmm/apps/trimmomatic/0.36
module use $HOME/tools/hisat2-2.1.0/
module load igmm/apps/samtools/1.6
module load igmm/apps/picard/2.17.11
module load igmm/apps/vcftools/0.1.13
module load igmm/apps/bcftools/1.6
module load anaconda

#Special conda environment for WASP version 0.3.4 (python 3 conversion)
source activate waspv0.3.4


#Remapping loop (REF sorted and Indexed)
cd $OUTPUT


ls -1 ./WASP/*/*_rehead.bam > array.list_$SGE_TASK_ID
infile=$(awk "NR==$SGE_TASK_ID" array.list_$SGE_TASK_ID)
BAMNAME=$(basename -s _rehead.bam $infile)

#Ref sort
java -jar /exports/igmm/software/pkg/el7/apps/picard/2.17.11/picard.jar \
ReorderSam \
REFERENCE=${REF}/ref/Sscrofa11.1.100.fa \
QUIET=true \
I=${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped.bam \
O=${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped_sorted.bam \
TMP_DIR=$TMP
	
# BuildBamIndex
java -jar /exports/igmm/software/pkg/el7/apps/picard/2.17.11/picard.jar \
BuildBamIndex \
QUIET=true \
I=${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped_sorted.bam \
O=${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped_sorted.bam.bai

source deactivate
rm -f ./array.list_*



