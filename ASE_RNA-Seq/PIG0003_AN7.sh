#!/bin/bash
#This is the script for the analysis 

#SGE flags
#$ -N PIG0003_AN7												##TBC1
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=20G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 4
#$ -V


#Abs paths
REF=/YOURFOLDERSTRUCTURE/workplace_pig
REF2=/YOURFOLDERSTRUCTURE/workplace_pig/ref/Sscrofa11.1.100
SOURCE=/YOURFOLDERSTRUCTURE/bams
mkdir -p /YOURFOLDERSTRUCTURE/$USER/PIG0003						##TBC2
WASP=/home/$USER/tools/WASP
DESTINATION=/YOURFOLDERSTRUCTURE/$USER/PIG0003					##TBC3

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/PIG0003_output								##TBC5
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=/exports/eddie/scratch/$USER/bams							##TBC6
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/PIG0003_output								##TBC7

#Loading modules
module load roslin/fastqc/0.11.7
module load igmm/apps/trimmomatic/0.36
module use $HOME/tools/hisat2-2.1.0/
module load igmm/apps/samtools/1.6
module load igmm/apps/picard/2.17.11
module load igmm/apps/vcftools/0.1.13
module load igmm/apps/bcftools/1.6
module load anaconda

#Special conda environment for WASP version 0.3.4 (python 3 conversion)
source activate waspv0.3.4



cd ${OUTPUT}

####Sample file name for RG manipulation
###f1_macrophage_LPS_time_course_0_hrs_ASE_ready_SO_rmdup.bam +++++++++++++ $(cut -d'_' -f3 <<< "$f")

for f in ${OUTPUT}/WASP/*/*_ASE_ready_SO_rmdup.bam;do
	FILENAME=$(basename -s _ASE_ready_SO_rmdup.bam $f)
	
	#Brining the ENSEMBL terminology back for GATK
	samtools view -H $f > tmp_header.sam
	sed -i -e '/SN:chr/ s/SN:chr/SN:/' tmp_header.sam 
	samtools reheader tmp_header.sam $f > tmp.bam
	
	java -jar /exports/igmm/software/pkg/el7/apps/picard/2.17.11/picard.jar \
	ReorderSam \
	REFERENCE=${REF}/ref/Sscrofa11.1.100.fa \
	QUIET=true \
	I=tmp.bam \
	O=tmp2.bam \
	TMP_DIR=$TMP

	#Name of the BAM file might change the column in the <<< syntax
	java -jar /exports/igmm/software/pkg/el7/apps/picard/2.17.11/picard.jar \
	AddOrReplaceReadGroups QUIET=true I=tmp2.bam O=tmp3.bam TMP_DIR=$TMP \
	RGID=$(cut -d'_' -f1 <<< "${FILENAME}") \
	RGSM=$(cut -d'_' -f2 <<< "${FILENAME}") \
	RGPL=illumina \
	RGLB=HD_frstrand \
	RGPU=C7HU5ANXX
	
	mv -f tmp3.bam $f	
	
	# BuildBamIndex
	java -jar /exports/igmm/software/pkg/el7/apps/picard/2.17.11/picard.jar \
	BuildBamIndex \
	QUIET=true \
	I=$f \
	O=$f.bai

	#cleanup
	rm -f tmp_header.sam tmp.bam tmp2.bam tmp3.bam
done;


mkdir -p ${OUTPUT}/ASE/

for a in ${OUTPUT}/WASP/*/*_ASE_ready_SO_rmdup.bam;do 
	VCFNAME=$(basename -s _ASE_ready_SO_rmdup.bam $a)

	mkdir -p ${OUTPUT}/ASE/${VCFNAME}/
	mkdir -p ${OUTPUT}/ASE/${VCFNAME}/ASE_Count/

	java -jar ${HOME}/tools/GenomeAnalysisTK.jar -T ASEReadCounter \
	-R ${REF}/ref/Sscrofa11.1.100.fa \	
	--sitesVCFFile ${REF}/Sscrofa11.1.100_SNV.vcf.gz \
	-I $a -o ${OUTPUT}/ASE/${VCFNAME}/ASE_Count/${VCFNAME}_ASERC.csv \
	-U ALLOW_N_CIGAR_READS \
	--outputFormat CSV \
	-mmq 50 \
	-mbq 25
done;

source deactivate
