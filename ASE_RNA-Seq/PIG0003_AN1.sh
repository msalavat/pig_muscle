#!/bin/bash
#This is the script for the analysis 

#SGE flags
#$ -N PIG0003_AN1									##TBC1
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=20G
#$ -r yes
#$ -notify 
trap 'exit 99' sigusr1 sigusr2 sigterm
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 4

#$ -V

#Abs paths
REF=/YOURFOLDERSTRUCTURE/workplace_pig
REF2=/YOURFOLDERSTRUCTURE/workplace_pig/ref/Sscrofa11.1.100		##TBC
mkdir -p //YOURFOLDERSTRUCTURE/$USER/PIG0003						##TBC2
WASP=/home/$USER/tools/WASP
DESTINATION=/YOURFOLDERSTRUCTURE/$USER/PIG0003					##TBC3

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/PIG0003_output							##TBC4
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=/YOURFOLDERSTRUCTURE/$USER/bams						##TBC5
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/PIG0003_output							##TBC6

#Loading modules
module load roslin/fastqc/0.11.7
module load igmm/apps/trimmomatic/0.36
module use $HOME/tools/hisat2-2.1.0/
module load igmm/apps/samtools/1.6
module load igmm/apps/picard/2.17.11
module load igmm/apps/vcftools/0.1.13
module load igmm/apps/bcftools/1.6
module load anaconda

#Special conda environment for WASP version 0.3.4 (python 3 conversion)
source activate waspv0.3.4

#WASP pre-loop
cd $OUTPUT
mkdir -p ${OUTPUT}/WASP

for b in ${INPUT}/*.bam;do							##TBC8 Name of the tissue and folder containing it
	
	BAMNAME=$(basename -s .bam $b)
	
	#Original output forlder
	mkdir -p ${OUTPUT}/WASP/${BAMNAME}

	#SNP file for WASP requires special treatment
	samtools view -H $b > tmp_header.sam
	sed -i -e '/SN:[0-9]/ s/SN:/SN:chr/' tmp_header.sam 
	samtools reheader tmp_header.sam $b > ${OUTPUT}/WASP/$BAMNAME/${BAMNAME}_rehead.bam
	
	#Synthesis (this folder name will be fed to array job by $(basename -s _rehead.bam $infile) in AN2 script
	mkdir -p ${OUTPUT}/WASP/${BAMNAME}/synthetic
	
done; 


source deactivate