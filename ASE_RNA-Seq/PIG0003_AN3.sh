#!/bin/bash
#This is the script for the analysis 

#SGE flags
#$ -N PIG0003_AN3													##TBC1
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=10G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 8
#$ -V


#Abs paths
REF=/YOURFOLDERSTRUCTURE/workplace_pig
REF2=/YOURFOLDERSTRUCTURE/workplace_pig/ref/Sscrofa11.1.100
SOURCE=/YOURFOLDERSTRUCTURE/bams
mkdir -p /YOURFOLDERSTRUCTURE/$USER/PIG0003						##TBC2
WASP=/home/$USER/tools/WASP
DESTINATION=/YOURFOLDERSTRUCTURE/$USER/PIG0003					##TBC3

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/PIG0003_output								##TBC5
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=/exports/eddie/scratch/$USER/bams							##TBC6
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/PIG0003_output								##TBC7

#Loading modules
module load roslin/fastqc/0.11.7
module load igmm/apps/trimmomatic/0.36
module use $HOME/tools/hisat2-2.1.0/
module load igmm/apps/samtools/1.6
module load igmm/apps/picard/2.17.11
module load igmm/apps/vcftools/0.1.13
module load igmm/apps/bcftools/1.6
module load anaconda

#Special conda environment for WASP version 0.3.4 (python 3 conversion)
source activate waspv0.3.4


#Remapping loop (REF sorted and Indexed)
cd $OUTPUT

for b in ${OUTPUT}/WASP/*/*_rehead.bam;do
	BAMNAME=$(basename -s _rehead.bam $b)
	
	#Re-Mapping and alignment
	hisat2 -x ${REF2} \
	--known-splicesite-infile ${REF}/ref/Sscrofa11.1.100_splice_sites.txt \
	-p 8 \
	-t \
	--met 1 \
	--rna-strandness RF \
	--dta-cufflinks \
	-1 ${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_rehead.remap.fq1.gz \
	-2 ${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_rehead.remap.fq2.gz | samtools view -@ 8 -bS | samtools sort -@ 8 -o ${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped.bam

done;

source deactivate
