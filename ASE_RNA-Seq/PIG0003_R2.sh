#!/bin/bash
#This is the script for the analysis 
#SGE flags
#$ -N PIG0003_R2							##TBC1
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=30G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 1
#$ -V
#$ -t 1-26								##TBC2	


#Abs paths
REF=/YOURFOLDERSTRUCTURE/workplace_pig
REF2=/YOURFOLDERSTRUCTURE/workplace_pig/ref/Sscrofa11.1.100
SOURCE=/YOURFOLDERSTRUCTURE/bams
mkdir -p /YOURFOLDERSTRUCTURE/$USER/PIG0003						##TBC2
WASP=/home/$USER/tools/WASP
DESTINATION=/YOURFOLDERSTRUCTURE/$USER/PIG0003					##TBC3

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/PIG0003_output								##TBC5
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=/exports/eddie/scratch/$USER/bams							##TBC6
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/PIG0003_output								##TBC7

#Loading modules
module load igmm/apps/R/3.6.0


cd ${OUTPUT}/GeneiASE_SVP/
ls -1 *_ASERC_SVP.txt > array.list_$SGE_TASK_ID
#infile=`sed -n -e "$SGE_TASK_ID p" array.list`
infile=$(awk "NR==$SGE_TASK_ID" array.list_$SGE_TASK_ID)
 
#GeneiASE static run
$HOME/tools/geneiase/bin/geneiase -t static -b 1e5 -x 100 -l $HOME/tools/geneiase/lib/static.lib.R  -i ${OUTPUT}/GeneiASE_SVP/${infile} -o ${OUTPUT}/GeneiASE_SVP/${infile::-14}/${infile::-14}_static.txt

rm -f array.list_*

##Trash collector
rm -Rf $OUTPUT/WASP/*/synthetic/
