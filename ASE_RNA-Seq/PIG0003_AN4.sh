#!/bin/bash
#This is the script for the analysis 

#SGE flags
#$ -N PIG0003_AN4												##TBC1
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=32G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 2
#$ -V
#$ -t 1-26													##TBC2


#Abs paths
REF=/YOURFOLDERSTRUCTURE/workplace_pig
REF2=/YOURFOLDERSTRUCTURE/workplace_pig/ref/Sscrofa11.1.100
SOURCE=/YOURFOLDERSTRUCTURE/bams
mkdir -p /YOURFOLDERSTRUCTURE/$USER/PIG0003						##TBC2
WASP=/home/$USER/tools/WASP
DESTINATION=/YOURFOLDERSTRUCTURE/$USER/PIG0003					##TBC3

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/PIG0003_output								##TBC5
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=/exports/eddie/scratch/$USER/bams							##TBC6
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/PIG0003_output								##TBC7

#Loading modules
module load roslin/fastqc/0.11.7
module load igmm/apps/trimmomatic/0.36
module use $HOME/tools/hisat2-2.1.0/
module load igmm/apps/samtools/1.6
module load igmm/apps/picard/2.17.11
module load igmm/apps/vcftools/0.1.13
module load igmm/apps/bcftools/1.6
module load anaconda

#Special conda environment for WASP version 0.3.4 (python 3 conversion)
source activate waspv0.3.4


#WASP filteration
cd $OUTPUT

#Creating the list of input BAM files for WASP array 
#The number of input files should match the requested e.g. -t 1-6 flag

ls -1 ./WASP/*/*_rehead.bam > array.list_$SGE_TASK_ID
#sed version
#infile=`sed -n -e "$SGE_TASK_ID p" array.list`
#awk version
infile=$(awk "NR==$SGE_TASK_ID" array.list_$SGE_TASK_ID)

#In each task only 1 infile should be selected hence uniq BAMNAMEs

BAMNAME=$(basename -s _rehead.bam $infile)

############ The ${infile::-11} == ${BAMNAME} in AN1 ###############

python ${WASP}/mapping/filter_remapped_reads.py \
	${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_rehead.to.remap.bam \
	${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped_sorted.bam \
	${OUTPUT}/WASP/${BAMNAME}/synthetic/${BAMNAME}_remapped_keep.bam

source deactivate
rm -f ./array.list_*
