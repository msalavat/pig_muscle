#!/bin/bash
#This is the script for the analysis 
#SGE flags
#$ -N PIG0003_R1										##TBC1
#$ -cwd
#$ -l h_rt=240:00:00
#$ -l h_vmem=30G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 1
#$ -V


#Abs paths
REF=/YOURFOLDERSTRUCTURE/workplace_pig
REF2=/YOURFOLDERSTRUCTURE/workplace_pig/ref/Sscrofa11.1.100
SOURCE=/YOURFOLDERSTRUCTURE/bams
mkdir -p /YOURFOLDERSTRUCTURE/$USER/PIG0003						##TBC2
WASP=/home/$USER/tools/WASP
DESTINATION=/YOURFOLDERSTRUCTURE/$USER/PIG0003					##TBC3

#Rel paths
mkdir -p $DESTINATION/tmp
mkdir -p $DESTINATION/PIG0003_output								##TBC5
SNP_DIR=$REF/WASP_SNP_DIR
INPUT=/exports/eddie/scratch/$USER/bams							##TBC6
TMP=$DESTINATION/tmp
OUTPUT=$DESTINATION/PIG0003_output								##TBC7

#Loading modules
module load igmm/apps/R/3.6.0


mkdir -p ${OUTPUT}/GeneiASE_SVP/
#R prep for GeneiASE
R CMD BATCH --no-save $HOME/Rscripts/R_prep_static_PIG0003.R $HOME/logs/PIG0003_R_prep_static.Rout	##TBC6 

for gs in ${OUTPUT}/ASE/*/ASE_Count/*_ASERC.csv;do 
	ASENAME=$(basename -s _ASERC.csv $gs)
	mkdir -p ${OUTPUT}/GeneiASE_SVP/${ASENAME}/
	
done;

