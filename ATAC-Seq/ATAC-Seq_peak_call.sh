#!/bin/bash
#This is the script for the analysis
#SGE flags
#$ -N genrichR        ##TBC1
#$ -cwd
#$ -l h_rt=24:00:00
#$ -l h_vmem=20G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 1
#$ -V
#$ -t 1-24

module load roslin/fastqc/0.11.7
module load roslin/samtools/1.9

cd /YOURFOLDERSTRUCTURE/bams_Genrich

ls -1 ./*.bam > array.list_$SGE_TASK_ID
infile=$(awk "NR==$SGE_TASK_ID" array.list_$SGE_TASK_ID)
Genrich -t ${infile} \
	-o ${infile/.bam/_peak.bed} \
	-f ${infile/.bam/_peak.log} \
	-z \
	-j \
	-r \
	-e MT \
	-v
rm -f array.list_$SGE_TASK_ID 






