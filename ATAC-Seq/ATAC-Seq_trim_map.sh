#!/bin/bash
 
#SGE flags
#$ -N ATAC_MAP                                  ##TBC1
#$ -cwd
#$ -l h_rt=96:00:00
#$ -l h_vmem=12G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 8
## -V
 
#Abs paths
INPUT=/YOURFOLDERSTRUCTURE/ATAC                                         
 
#Rel paths
mkdir -p ${INPUT}/trimmed
mkdir -p ${INPUT}/bams
mkdir -p ${INPUT}/tmp
 
#Loading modules
module load python/3.4.3
module load roslin/fastqc/0.11.7
module load igmm/apps/cutadapt/1.9.1
module load roslin/samtools/1.9
module load igmm/apps/bwa/0.7.16
module load igmm/apps/picard/2.17.11
module load igmm/apps/pigz/2.3.3
module load trimmomatic/0.39

 
cd $INPUT
pwd
 

#Trimming
for f in ./trimmed/*_R1.fastq.gz;do
    NAME=$(basename -s _R1.fastq.gz ${f})
    java -jar trimmomatic-0.39.jar PE -threads 8 ${f} ${f/_R1.fastq.gz/_R2.fastq.gz} \
    ${INPUT}/trimmed/${NAME}_1P.fq.gz \
    ${INPUT}/trimmed/${NAME}_1U.fq.gz \
    ${INPUT}/trimmed/${NAME}_2P.fq.gz \
    ${INPUT}/trimmed/${NAME}_2U.fq.gz \
    ILLUMINACLIP:Trimmomatic-0.39/adapters/NexteraPE-PE.fa:2:30:10:1:true \
    MINLEN:30
done;
  
for b in ${INPUT}/trimmed/*_1P.fq.gz;do
    BAMNAME=$(basename -s _1P.fq.gz ${b})
    bowtie2  --very-sensitive -p 8 -x Sscrofa11.1_bowtie2 -1 ${b}  -2 ${b/_1P.fq.gz/_2P.fq.gz} \
    | samtools view  -@8 -bS -F 4 \
    | samtools sort -@8 -o ${INPUT}/bams/${BAMNAME}.bam
    java -jar picard.jar MarkDuplicates I=${INPUT}/bams/${BAMNAME}.bam O=${INPUT}/bams/${BAMNAME}_dd.bam M=${INPUT}/bams/${BAMNAME}_min30bp_dd_metrics
    rm -f ${INPUT}/bams/${BAMNAME}.bam
done;

