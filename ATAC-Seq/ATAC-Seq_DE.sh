#!/bin/bash
#SGE flags

#$ -N sub_bammer
#$ -cwd
#$ -l h_rt=06:00:00
#$ -l h_vmem=8G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 4
#$ -V
 
#Abs paths
INPUT=/YOURFOLDERSTRUCTURE/ATAC                                         
 
#Loading modules
module load samtools/1.9
module load pigz/2.3.3
module load anaconda/5.0.1
module load BEDTools/2.30.0

 
cd $INPUT

#Subsetting the bam files for high quality mapped reads excluding SAM flag 1548:
#Summary:
#    read unmapped (0x4)
#    mate unmapped (0x8)*
#    read fails platform/vendor quality checks (0x200)
#    read is PCR or optical duplicate (0x400)

for b in bams/*.bam;do
	NAME=$(basename -s .bam $b)
	samtools view -h -f2 -q10 -F1548 -@4 -bS $b -L bams/regions.bed > sub_bams/${NAME}_HQ.bam
	samtools index -@4 sub_bams/${NAME}_HQ.bam
done;

# Creating a pileup set (all in one [aio]) of peaks using the peaks called by Genrich for each and every sample (n=24)
for b in peaks/*.bed.gz;do
	NAME=$(basename -s _dd_qsort_peak.bed.gz $b)
	zcat $b | awk -v NAME=$NAME '{OFS="\t";$4=NAME}1' >> aio.bed
done;

#Sorting the output all in one set of peaks by coordinate
sort -k1,1 -k2,2n aio.bed > aio_sort.bed

#Fourth, seventh and tenth columns (name of the animal, AUC score of the peak and peak position in bp from start [0 based]) were used for merger process columns 4-6
# A 10 bp gap between book  end regions was considered for merging them in to 1 region (bedtools terminology). 
# The fourth column can be then be used for tissue support metrics (max n=24)
# A total of 19,368 regions were created using the consensus process

bedtools merge -i aio_sort.bed -d 10 -c 4,7,10,4 -o count_distinct,mean,mode,distinct | \
pigz > aio_sort_merged_D10bpGap_C4countDistinctID_C7meanScore_C10peakMode.bed.gz

#Unique peaks supported by at least 3 muscle tissues (support >=3) were chosen as the "consensus set"
zcat aio_sort_merged_D10bpGap_C4countDistinctID_C7meanScore_C10peakMode.bed.gz | \
awk '$4>=3' | \
pigz > aio_min3_support.bed.gz #the resulting file exists in this repository

#Converting the final bed format to GFF3 for being used in the counting step (ht-Seq)
zcat aio_min3_support.bed.gz | \
awk 'OFS="\t" {print $1,"Genrich_ATAC","region",$2,$3,$5,".",".","ID=concensus_peak"FNR"_support_"$4";peakPos="$6";supports="$7}' | \
sed 's/,/\|/g' |  \
awk '$4!=0' | \
pigz  > aio_min3_support.gff.gz #the resulting file exists in this repository 


# Counting read fragments based on the consensus ATAC-seq set (min 3 animals and 12090 peaks)
# A separate task array job was used for the ht-Seq counting

#!/bin/bash
#SGE flags

#$ -N htseqer
#$ -cwd
#$ -l h_rt=24:00:00
#$ -l h_vmem=16G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 1
#$ -V
#$ -t 1-24

#Abs paths
INPUT=/YOURFOLDERSTRUCTURE/ATAC                                         
 
#Loading modules
module load samtools/1.9
module load pigz/2.3.3
module load anaconda/5.0.1
module load BEDTools/2.30.0

 
cd $INPUT
mkdir -p ./counts
source activate htseqv0.13.5

ls -1 ./sub_bams/*.bam > array.list_$SGE_TASK_ID
inFile=$(awk "NR==$SGE_TASK_ID" array.list_$SGE_TASK_ID)
outName=$(basename -s _dd_HQ.bam $inFile)
htseq-count --stranded=no --type=region ${inFile} aio_min3_support.gff.gz > ./counts/${outName}.counts

source deactivate
rm -f array.list_$SGE_TASK_ID
