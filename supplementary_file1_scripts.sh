#Bootstraping the interesect function of bedTools for 3 phenotype groups in day 90 samples (n=12)
bedtools intersect -sortout -u -a D90Small_peak.bed -b D90Average_peak.bed D90Large_peak.bed > SmalltoAll.bed
bedtools intersect -sortout -u -a D90Average_peak.bed -b D90Small_peak.bed D90Large_peak.bed > AveragetoAll.bed
bedtools intersect -sortout -u -a D90Large_peak.bed -b D90Small_peak.bed D90Average_peak.bed > LargetoAll.bed

#The final merger for the inner joing of all 3 phenotype groups
cat SmalltoAll.bed AveragetoAll.bed LargetoAll.bed | sort -V -k1 -k2,3 | uniq > inner.bed

#Subtracting the phenotype specific peaks at day 90 usign the subtract function of bedTools
bedtools subtract -A -a D90Small_peak.bed -b inner.bed > D90Small_specific_peak.bed
bedtools subtract -A -a D90Average_peak.bed -b inner.bed > D90Average_specific_peak.bed
bedtools subtract -A -a D90Large_peak.bed -b inner.bed > D90Large_specific_peak.bed
