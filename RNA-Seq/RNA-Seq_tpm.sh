#!/bin/bash
#This is the script for the analysis
#SGE flags
#$ -N RNASeqWRAP        ##TBC1
#$ -cwd
#$ -l h_rt=24:00:00
#$ -l h_vmem=20G
#$ -o $HOME/logs/
#$ -e $HOME/logs/
#$ -pe sharedmem 6
#$ -V
#$ -t 1-25
## -tc 5

#The correct folder structure for the analysis of choice. 
RUN_PATH=/YOURFOLDERSTRUCTURE/
vCPU=6

mkdir -p $RUN_PATH/Reports
mkdir -p $RUN_PATH/dump
mkdir -p $RUN_PATH/index
REPORTS=$RUN_PATH/Reports 

module load sratoolkit/2.8.2-1
module load fastqc/0.11.7
module load trimmomatic/0.39
module load kallisto/0.44.0
module load anaconda
module load pigz/2.3.3


#list of the fastq files in dump folder. Each sample placed in a subfolder (2 fastq files)
ls -1 ${RUN_PATH}/dump > ${RUN_PATH}/inlist_${SGE_TASK_ID}
sra_id=$(awk "NR==${SGE_TASK_ID}" ${RUN_PATH}/inlist_${SGE_TASK_ID})
file1=$(basename -a ${RUN_PATH}/dump/${sra_id}/*_1.fastq.gz)
file2=$(basename -a ${RUN_PATH}/dump/${sra_id}/*_2.fastq.gz)


# run fastqc on raw data
fastqc -t $((${vCPU}/2)) ${RUN_PATH}/dump/${sra_id}_1.fastq.gz -o $REPORTS &\
fastqc -t $((${vCPU}/2)) ${RUN_PATH}/dump/${sra_id}_2.fastq.gz -o $REPORTS

# trim reads with trimmomatic
NAME=$(basename -s _R1.fq.gz ${RUN_PATH}/dump/${sra_id}_1.fq.gz)
java -jar trimmomatic/0.36/trimmomatic-0.39.jar PE -threads ${vCPU} \
-basein ${RUN_PATH}/dump/${sra_id}_1.fastq.gz \
-baseout ${RUN_PATH}/dump/${sra_id}_trimmed.fq.gz \
ILLUMINACLIP:Trimmomatic-0.39/adapters/TruSeq3-PE-2.fa:2:30:10:1:TRUE SLIDINGWINDOW:5:20 MINLEN:50

# remove raw data
rm -f ${RUN_PATH}/dump/${sra_id}_1.fastq.gz ${RUN_PATH}/dump/${sra_id}_2.fastq.gz

# run fastqc on trimmed data

fastqc -t $((${vCPU}/2)) ${RUN_PATH}/dump/${sra_id}_trimmed_1P.fq.gz -o $REPORTS &\
fastqc -t $((${vCPU}/2)) ${RUN_PATH}/dump/${sra_id}_trimmed_2P.fq.gz -o $REPORTS

# remove the unpaired reads
rm -f ${RUN_PATH}/dump/${sra_id}_trimmed_1U.fq.gz ${RUN_PATH}/dump/${sra_id}_trimmed_2U.fq.gz

	
# checking the presence of indexes
if [[ -f "${RUN_PATH}/index/Ssc_v11.1.idx" ]];then
		echo "All set!"
	else
		function build_indexes(){
    		kallisto index -i ${RUN_PATH}/index/Ssc_v11.1.idx ${RUN_PATH}/index/Sus_scrofa.Sscrofa11.1.cds.all.fa.gz
		}
		build_indexes
fi;

# Kalisto quantification
Tfile="${RUN_PATH}/dump/${sra_id}_trimmed_1P.fq.gz"
mkdir -p $RUN_PATH/${sra_id}
mkdir -p $RUN_PATH/${sra_id}/Kallisto_out
kallisto quant --bias -t ${vCPU} -i $RUN_PATH/index/Ssc_v11.1.idx -o $RUN_PATH/${sra_id}/Kallisto_out <(zcat ${Tfile}) <(zcat ${Tfile/_1P.fq.gz/_2P.fq.gz})


# remove the paired reads
rm -f ${RUN_PATH}/dump/${sra_id}_trimmed_1P.fq.gz ${RUN_PATH}/dump/${sra_id}_trimmed_2P.fq.gz
